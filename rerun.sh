#!/bin/sh

function check_file()
{
    i=0;
    while [ ! -f $1 ]; do 
        i=`expr $i + 1`;
        if [ $i -gt 10 ]; then
            echo $1 'not exists, exit. FATAL!' >> $gender_home/log.txt
            exit 1
        fi
        sleep 1;
    done
    echo $1' exists, continue!' 
}

target_date=$1
gender_home='/usr/local/vntop/gender-prediction_xgboost'
#gender_home='/home/bentan/gender_prediction_test'
mnt_home='~/xb-data/user_gender_coverall'
rm -rf $gender_home/data/*.csv

echo 'start training at '`date +%Y%m%d-%H:%M:%S` 'for' $target_date >> $gender_home/log.txt

echo 'get device info' >> $gender_home/log.txt
$gender_home/get_device_info.py $gender_home/data/device_info.csv
check_file $gender_home/data/device_info.csv

echo 'get user login info which contains gender nickname' >> $gender_home/log.txt
$gender_home/get_user_gender.py $gender_home/data/user_login_info.csv
check_file $gender_home/data/user_login_info.csv

echo 'remove unusual nickname' >> $gender_home/log.txt
./remove_unsual_nickname.py $gender_home/data/user_login_info.csv $gender_home/data/user_login_info_clean.csv
check_file $gender_home/data/user_login_info_clean.csv

start_date=`date -d "$target_date 8 days ago" "+%Y%m%d"`
end_date=`date -d "$target_date 1 days ago" "+%Y%m%d"`
end_date_hour=`date -d "$target_date 1 days ago" "+%Y%m%d%H"`

echo 'get user register info' >> $gender_home/log.txt
$gender_home/get_user_info_between.py $start_date $end_date $gender_home/data/user_info.csv
check_file $gender_home/data/user_info.csv

echo 'get active user list' >> $gender_home/log.txt
$gender_home/get_active_user_between.py $start_date $end_date $gender_home/data/active_user.csv
check_file $gender_home/data/active_user.csv

echo 'get user content summary' >> $gender_home/log.txt
$gender_home/get_user_content_between.py $start_date $end_date $gender_home/data/user_content_summary.csv
check_file $gender_home/data/user_content_summary.csv
echo 'get applist' >> $gender_home/log.txt
$gender_home/get_user_applist_between.py $start_date $end_date $gender_home/data/user_applist.csv
echo 'create training and test dataset' >> $gender_home/log.txt
$gender_home/create_gender_dataset.py $gender_home/data/training.csv $gender_home/data/test.csv
check_file $gender_home/data/training.csv
check_file $gender_home/data/test.csv

echo 'training gender model and make prediction' >> $gender_home/log.txt
$gender_home/train_gender_model_gbdt_rf.py $gender_home/data/training.csv $gender_home/data/test.csv $end_date_hour $gender_home/log.txt ${mnt_home}_log 
check_file $gender_home/result/user_gender_pred_${end_date_hour}_cover_month.csv

#rm -rf $mnt_home/daily_result/user_gender_pred_${end_date}_cover_month.csv
#cp $gender_home/result/user_gender_pred_${end_date}_cover_month.csv $mnt_home/daily_result/user_gender_pred_${end_date}_cover_month.csv

echo 'append the result to the previous one' >> $gender_home/log.txt
#$gender_home/combine_results.py $gender_home/result/user_gender_pred_cover_month.csv  $gender_home/result/user_gender_pred_${end_date_hour}_cover_month.csv $gender_home/data/user_login_info.csv $gender_home/data/active_user.csv $gender_home/result/coverage_stat.csv
$gender_home/combine_results.py $gender_home/result/user_gender_pred_cover_month.csv  $gender_home/result/user_gender_pred_${end_date_hour}_cover_month.csv $gender_home/result/user_gender_best_pred_month.csv  $gender_home/result/user_gender_best_pred_${end_date_hour}_cover_month.csv
#rm -rf $mnt_home/user_gender_pred_cover_month.csv
#cp $gender_home/result/user_gender_pred_cover_month.csv $mnt_home/user_gender_pred_cover_month.csv

num=`cat $gender_home/result/user_gender_pred_cover_month.csv | wc -l`
nowtime=`date +%Y%m%d-%H:%M:%S`
echo 'total user: '"$num $nowtime" >>  $gender_home/log.txt
echo 'end training and predicting at '`date +%Y%m%d-%H:%M:%S` >> $gender_home/log.txt
echo '----------------------' >> $gender_home/log.txt

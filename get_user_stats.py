#!/usr/bin/python

import pandas as pd
from collections import Counter
import sys
import datetime

def get_date(my_str):
    
    date_str = my_str.split(' ')[0]
    return datetime.datetime.strptime(date_str, '%Y-%m-%d').strftime('%Y%m%d')

if __name__=="__main__":

    if len(sys.argv) < 3:
        print('please specify the date and the output file!')
        exit()

    date_str = sys.argv[1].strip()
    user_info_file = "user_info_day_%s.csv"%(date_str)
    user_action_file = "user_action_week_%s.csv"%(date_str)
    user_unreg_file = "user_unreg_month_%s.csv"%(date_str)
    out_put_file = sys.argv[2].strip()
    user_info = pd.read_csv(user_info_file, sep='|')
    user_action = pd.read_csv(user_action_file, sep='|')
    user_unreg = pd.read_csv(user_unreg_file, sep='|')
    
    out_file = open(out_put_file, 'a')
    out_file.write(date_str + '\n')
   
    user_list = user_info['id'].unique()
    num_new_user = len(user_list)
    out_file.write('# of new registed users: ' + str(num_new_user) + '\n')
    active_user_list = user_action['user_id'].unique() 
    num_active_user = len(active_user_list)
    out_file.write('# of active users in 7 days: ' + str(num_active_user) + '\n')
    out_file.write('# of unactive users: ' + str(num_new_user - num_active_user) + '\n')
    unreg_user_list = user_unreg['push_id'].unique()
    num_unreg_user = len(unreg_user_list)
    out_file.write('# of lost users in 31 days: ' + str(num_unreg_user) + '\n')
    out_file.write('# of remaining users: ' + str(num_new_user - num_unreg_user) + '\n')
    
   
    user_action['hive_ts'] = user_action['ts'].apply(get_date)
    date_list = user_action['hive_ts'].unique()
    user_action = user_action.drop('ts', 1)
    date_list.sort()
    for date in date_list:
        user_day_action = user_action.loc[user_action['hive_ts']==date]
        #user_day_action.rename(columns={'user_push_id':'push_id'}, inplace=True)
        out_file.write('# of users login at ' + str(date) + ' :' + str(len(user_day_action['user_id'].unique())))
        out_file.write('\n')
        #user_unreg_day = pd.merge(user_day_action, user_unreg, how='inner', on=['push_id'])
        #out_file.write('# of users login at ' + str(date) + ' :' + str(len(user_day_action['user_id'].unique())))
        
    
    user_freq = Counter(user_action[['user_id', 'hive_ts']].drop_duplicates().groupby('user_id').size())
    day_list = user_freq.keys()
    day_list.sort()
    user_cnt_list = user_action[['user_id', 'hive_ts']].drop_duplicates().\
                                    groupby('user_id').size().reset_index(name='cnt')
    print('0num of users' + str(len(user_cnt_list['user_id'].unique())) + '\n')
     

    user_info.rename(columns={'id':'user_id', 'user_push_id':'push_id'}, inplace=True)
    print('1num of users' + str(len(user_info['user_id'].unique())) + '\n')
    user_info = pd.merge(user_info, user_unreg, how='left', on=['push_id'])
    print('2num of users' + str(len(user_info['user_id'].unique())) + '\n')
    user_info['result'].fillna('Registered', inplace=True)
    
    user_info = pd.merge(user_info, user_cnt_list, how='inner', on=['user_id'])
    print('3num of users' + str(len(user_info['user_id'].unique())) + '\n')
    for day in day_list:
        out_file.write('# of number users login ' + str(day) + 'days: ' + str(user_freq[day]))
        user_info_part = user_info.loc[user_info['cnt'] == day]
        num_reg_info = user_info_part.groupby('result')['push_id'].nunique()
        #print(num_reg_info)
        out_file.write(' and ' + str(num_reg_info['Registered']) +' remain active and '\
                       +str(num_reg_info['NotRegistered']) + ' lost(' \
                       + str(int(num_reg_info['NotRegistered']*100.0/user_freq[day])) + '%)\n')
    
    out_file.write('\n-------------------\n\n')
    out_file.flush()
    out_file.close()

#!/usr/bin/python
import pandas as pd
import sys
import datetime

gender_home = '/usr/local/vntop/gender-prediction_xgboost/'
cluster_home = '/home/bentan/applistuserclustering/'
def get_date(my_str):
    date_str = my_str.split(' ')[0]
    return datetime.datetime.strptime(date_str, '%Y-%m-%d').strftime('%Y%m%d')


def combine_data():
    active_user = pd.read_csv(gender_home+'data/active_user.csv', sep='|', low_memory=False)
    active_user['user_id'] = active_user['user_id'].astype(str)
    user_info = pd.read_csv(gender_home+'data/user_info.csv', sep='|', low_memory=False)
    user_info.rename(columns={'id': 'user_id'}, inplace=True)
    user_info['user_id'] = user_info['user_id'].astype(str)
    device_info = pd.read_csv(gender_home+'data/device_info.csv', sep='|')
    user_login_info = pd.read_csv(gender_home+'data/user_login_info_clean.csv', sep='|')
    user_applist = pd.read_csv(cluster_home+'result/user_cluster.csv', sep=',')
    user_login_info.drop(['nickname'], axis=1, inplace=True)
    user_login_info['user_id'] = user_login_info['user_id'].astype(str)
    user_applist_orig_info = pd.read_csv(gender_home+'data/user_applist.csv', sep=',')
    user_applist_orig_info['user_id'] = user_applist_orig_info['user_id'].astype(str)
    user_content_info = pd.read_csv(gender_home+'data/user_content_summary.csv', sep='|')
    user_content_info['user_id'] = user_content_info['user_id'].astype(str)
    user_content_info['read_times_cnt'] = user_content_info.groupby('user_id')['read_times'].transform('sum')
    user_content_info['ctr'] = user_content_info['read_times'].astype(float) / user_content_info['rcmd_times']
    user_content_info['read_cat_ratio'] = \
        user_content_info['read_times'].astype(float) / user_content_info['read_times_cnt']
    user_content_info.fillna(value={'read_cat_ratio': 0}, inplace=True)

    # flat user ctr
    user_ctr = user_content_info.pivot('user_id', 'category_id', 'ctr').reset_index()
    user_ctr.columns = ["_".join(('ctr_cat', str(x))) for x in user_ctr.columns]
    user_ctr.rename(columns={'ctr_cat_user_id': 'user_id'}, inplace=True)
    user_ctr.fillna(0, inplace=True)

    # flat user read ratio
    user_read_ratio = user_content_info.pivot('user_id', 'category_id', 'read_cat_ratio').reset_index()
    user_read_ratio.columns = ["_".join(('read_ratio_cat', str(x))) for x in user_read_ratio.columns]
    user_read_ratio.rename(columns={'read_ratio_cat_user_id': 'user_id'}, inplace=True)
    user_read_ratio.fillna(0, inplace=True)
    # applist
    user_applist['user_id'] = user_applist['user_id'].astype(str)

    my_user_info = pd.merge(active_user, user_info, how='inner', on='user_id')
    my_user_info = pd.merge(my_user_info, device_info, how='left', on='device_model')
    my_user_info = pd.merge(my_user_info, user_ctr, how='left', on='user_id')
    my_user_info = pd.merge(my_user_info, user_read_ratio, how='left', on='user_id')
    my_user_info = pd.merge(my_user_info, user_login_info, how='left', on='user_id')
    my_user_info = pd.merge(my_user_info, user_applist, how='left', on='user_id')
    my_user_info = pd.merge(my_user_info, user_applist_orig_info, how='left', on='user_id')
    # my_user_info.to_csv('my_user_info.csv', index=False, sep='|')

    return my_user_info


def create_gender_dataset(user_info, out_put_train, out_put_test):
    # user_info_for_train = user_info.query['gender == male | gender == female']
    # user_info_for_test =user_info.query['gender != male & gender != female']
    user_info_for_train = user_info[(user_info.gender == 'male') | (user_info.gender == 'female')]
    user_info_for_test = user_info[(user_info.gender != 'male') & (user_info.gender != 'female')]
    user_info_for_train.to_csv(out_put_train, sep='|', index=False)
    user_info_for_test.to_csv(out_put_test, sep='|', index=False)


if __name__ == "__main__":

    if len(sys.argv) < 3:
        print('please specify the output files for training and test data.!')
        exit()

    user_info = combine_data()
    create_gender_dataset(user_info, sys.argv[1].strip(), sys.argv[2].strip())

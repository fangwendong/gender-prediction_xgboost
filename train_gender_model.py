#!/usr/bin/python
# coding=utf-8

import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier
from xgboost import XGBClassifier
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score
from sklearn import metrics
from sklearn.metrics import accuracy_score, recall_score, precision_score
import sys


def get_best_parameter(data, learning_rate, n_estimators, max_depth, high_th, low_th, mid_th):
    X_ = data.drop(['user_id', 'gender'], axis=1)
    X_ = X_.apply(lambda x: pd.factorize(x)[0])
    y_ = data[['gender']]
    y_.loc[y_['gender'] == 'male', 'gender'] = 1
    y_.loc[y_['gender'] == 'female', 'gender'] = 0

    X_train, X_test, y_train, y_test = train_test_split( \
        X_, y_, test_size=0.3, random_state=0)

    parameter = []
    th_acc = []
    all_acc = []
    all_auc = []
    # for lr, ne, md in [(lr, ne, md) for lr in learning_rate for ne in n_estimators for md in max_depth]:
    # clf_gbdt = GradientBoostingClassifier(loss='deviance', learning_rate=lr, \
    #                                       n_estimators=ne, max_depth=md, min_samples_leaf=3)
    # clf_gbdt.fit(X_train, y_train.astype('int'))
    #
    # y_pred_gbdt = clf_gbdt.predict_proba(X_test)[:, 1]
    clf_xgboost = XGBClassifier(max_depth=3, learning_rate=0.1, n_estimators=100)
    clf_xgboost.fit(X_train, y_train.astype('int'))
    y_pred_gbdt = clf_xgboost.predict_proba(X_test)[:, 1]
    y_test['result'] = y_pred_gbdt

    step = 0.002
    th_tmp = 0.6
    best_female_th = 0.5
    max_nums = 0
    while th_tmp > 0.2:
        th_tmp -= step
        if y_test.loc[(y_test['result'] <= th_tmp)].shape[0] <= 0:
            break
        female_num = y_test.loc[(y_test['result'] <= th_tmp) & (y_test['gender'] == 0)].shape[0]
        male_num = y_test.loc[(y_test['result'] <= th_tmp) & (y_test['gender'] == 1)].shape[0]
        acc = female_num / float(female_num + male_num)
        print('tmp female', th_tmp, acc, male_num, female_num)
        if acc > 0.8 and male_num > max_nums:
            best_female_th = th_tmp
            max_nums = male_num
            print(best_female_th, max_nums)

    th_tmp = 0.6
    best_male_th = 0.5
    max_nums = 0
    while th_tmp < 0.8:
        th_tmp += step
        if y_test.loc[(y_test['result'] >= th_tmp)].shape[0] <= 0:
            break
        female_num = y_test.loc[(y_test['result'] >= th_tmp) & (y_test['gender'] == 0)].shape[0]
        male_num = y_test.loc[(y_test['result'] >= th_tmp) & (y_test['gender'] == 1)].shape[0]
        acc = male_num / float(female_num + male_num)
        print('tmp male', th_tmp, acc, male_num, female_num)
        if acc > 0.8 and male_num > max_nums:
            best_male_th = th_tmp
            max_nums = male_num
            print(best_male_th, max_nums)

    # get accuracy of data within given threshold
    y_pred = y_test.loc[(y_test['result'] >= high_th) | (y_test['result'] <= low_th)]
    y_pred['final'] = y_pred['result']
    y_pred.loc[y_test['result'] >= high_th, 'final'] = 1
    y_pred.loc[y_test['result'] <= low_th, 'final'] = 0
    y_truth = y_pred['gender'].astype(int)
    y_model = y_pred['final'].astype(int)
    print("predicted users:" + str(y_pred.shape[0]))
    if (y_pred.shape[0] < 15):
        th_accuracy = 0.0
    else:
        th_accuracy = accuracy_score(y_truth, y_model)

    # get accuracy of all data
    y_pred = y_test
    y_pred['final'] = y_pred['result']
    y_pred.loc[y_test['result'] >= mid_th, 'final'] = 1
    y_pred.loc[y_test['result'] < mid_th, 'final'] = 0
    y_truth = y_pred['gender'].astype(int)
    y_model = y_pred['final'].astype(int)
    all_accuracy = accuracy_score(y_truth, y_model)
    auc = roc_auc_score(y_truth, y_pred['result'])
    parameter.append([3,0.1,100])
    th_acc.append(th_accuracy)
    all_acc.append(all_accuracy)
    all_auc.append(auc)


    si = np.argmax(th_acc)
    print(th_acc)
    print(all_acc)
    print(all_auc)
    print(parameter)
    return th_acc[si], all_acc[si], all_auc[si], parameter[si], [best_male_th, best_female_th]


def train_and_test_model(data, learning_rate, n_estimators, max_depth):
    X_ = data.drop(['user_id', 'gender'], axis=1)
    X_ = X_.apply(lambda x: pd.factorize(x)[0])
    y_ = data[['gender']]
    y_.loc[y_['gender'] == 'male', 'gender'] = 1
    y_.loc[y_['gender'] == 'female', 'gender'] = 0
    num_male = int(np.sum(y_))
    num_female = int(y_.shape[0] - num_male)

    X_train, X_test, y_train, y_test = train_test_split( \
        X_, y_, test_size=0.1, random_state=0)
    print('training size: ' + str(len(X_)) + ' (male: ' + \
          str(num_male) + ' female: ' + str(num_female) + \
          '), test size:' + str(len(X_test)) + '\n')

    clf_gbdt = GradientBoostingClassifier(loss='deviance', learning_rate=learning_rate, \
                                          n_estimators=n_estimators, max_depth=max_depth, min_samples_leaf=3)
    clf_gbdt.fit(X_train, y_train.astype('int'))

    y_pred_gbdt = clf_gbdt.predict_proba(X_test)[:, 1]
    y_test['result'] = y_pred_gbdt

    # get accuracy of data within given threshold
    y_pred = y_test.loc[(y_test['result'] >= 0.7) | (y_test['result'] <= 0.5)]
    y_pred['final'] = y_pred['result']
    y_pred.loc[y_test['result'] >= 0.7, 'final'] = 1
    y_pred.loc[y_test['result'] <= 0.5, 'final'] = 0
    y_truth = y_pred['gender'].astype(int)
    y_model = y_pred['final'].astype(int)
    th_accuracy = accuracy_score(y_truth, y_model)
    print("predicted users:" + str(y_pred.shape[0]))

    # get accuracy of all data
    y_pred = y_test
    y_pred['final'] = y_pred['result']
    y_pred.loc[y_test['result'] >= 0.6, 'final'] = 1
    y_pred.loc[y_test['result'] < 0.6, 'final'] = 0
    y_truth = y_pred['gender'].astype(int)
    y_model = y_pred['final'].astype(int)
    all_accuracy = accuracy_score(y_truth, y_model)

    print("threshold accuracy:" + str(th_accuracy))
    print("all accuracy:" + str(all_accuracy))
    return [th_accuracy, all_accuracy]


if __name__ == "__main__":
    training = sys.argv[1].strip()
    data = pd.read_csv(training, sep='|')
    # [th_acc, all_acc] = train_and_test_model(data, 0.1, 100, 3)
    [th_acc, all_acc, para] = get_best_parameter(data, [0.01, 0.05, 0.1], [100, 200, 300], [3, 4, 5], 0.7, 0.5, 0.6)

    print(th_acc)
    print(all_acc)
    print(para)

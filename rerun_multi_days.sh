#!/bin/sh

startDay=$1
endDay=$2

while (( $startDay != $endDay ))
do
    echo ${startDay}
    startDay=`date -d "+2 day ${startDay}" +%Y%m%d`
    echo ${startDay}
    ./rerun.sh ${startDay} >>rerun_info.log 2>&1
done

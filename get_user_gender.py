#!/usr/bin/python
import prestodb
import sys
import datetime
import config as cfg

'''
get user gender information of the day:
./get_user_gender.py output_file
'''

if __name__=="__main__":

    if len(sys.argv) < 2:
        print('please specify the output file!')
        exit()

    out_put_file = sys.argv[1]

    conn=prestodb.dbapi.connect(host=cfg.host,\
       port=cfg.port,user=cfg.user,catalog=cfg.catalog,schema=cfg.schema)
    cur = conn.cursor()
    my_sql = "select org_user_id as user_id, nickname, gender from login_user_tbl where \
              gender in (\'male\', \'female\')"
    print(my_sql)
    cur.execute(my_sql)
    rows = cur.fetchall()
    out_file = open(out_put_file, 'w')
    columns = ['user_id', 'nickname', 'gender']
    out_file.write('|'.join(columns))
    out_file.write('\n')
    for item in rows:
        #print(item)
        out_file.write('|'.join(unicode(v).encode('utf-8','ignore').strip() for v in item))
        out_file.write('\n')

    out_file.flush() 
    out_file.close()
    print('Done')

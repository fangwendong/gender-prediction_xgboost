#!/usr/bin/python
import prestodb
import sys
import datetime
import config as cfg
'''
get active user coverage:
./get_coverage.py last_day output_file
'''
if __name__=="__main__":

    if len(sys.argv) < 3:
        print('please specify the start date, end date and the output file!')
        exit()

    last_day = sys.argv[1].strip()
    out_put_file = sys.argv[2].strip()

    conn=prestodb.dbapi.connect(host=cfg.host,\
       port=cfg.port,user=cfg.user,catalog=cfg.catalog,schema=cfg.schema)
    cur = conn.cursor()
    my_sql = '''
with t_users as (

    select distinct user_id from default.user_daily_duration_stat
    where date_str>='%s'
),

t_user_tags as (
    select a.gender, a.gender_precision from dmp.user_tags a  
    join t_users b on cast(a.user_id as bigint) = b.user_id
),

gender_cnt as  (
select gender_precision, gender, count(1) as user_count from t_user_tags
group by gender_precision, gender
),
gender_sum as (
select sum(case when gender_precision!='0.6' then user_count else 0 end) as coverage_user_cnt,
sum(user_count) as active_user_num
from gender_cnt
)

select 
coverage_user_cnt,
active_user_num,
coverage_user_cnt/(0.0000+active_user_num) as rate

from gender_sum
'''%(last_day, )
    print(my_sql)
    cur.execute(my_sql)
    rows = cur.fetchall()
    out_file = open(out_put_file, 'a')
    for item in rows:
        #print(item)
        out_file.write(','.join(unicode(v).encode('utf-8','ignore').strip() for v in item))
        out_file.write(','+ last_day)
        out_file.write('\n')
        print('active user coverage', last_day, str(item))
    out_file.flush()
    out_file.close()
    print('Done')
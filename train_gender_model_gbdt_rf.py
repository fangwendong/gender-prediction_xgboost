#!/usr/bin/python

import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier
from xgboost import XGBClassifier
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score
from sklearn import metrics
from sklearn.metrics import accuracy_score, recall_score, precision_score
from train_gender_model import train_and_test_model, get_best_parameter
import sys
import time

start_time = time.time()

training = sys.argv[1].strip()
testing = sys.argv[2].strip()
today = sys.argv[3].strip()
local_log = sys.argv[4].strip()
mnt_log = sys.argv[5].strip()
local_log_out = open(local_log, 'a')

data_train = pd.read_csv(training, sep='|')
data_test = pd.read_csv(testing, sep='|')

learning_rate = [0.01]
n_estimators = [90]
max_depth = [3]
high_th = 0.7
low_th = 0.5
mid_th = 0.6

create_time = time.strftime('%Y%m%d%H')
local_log_out.write('start to get best parameter!\n')
th_acc, all_acc, all_auc, best_para, best_th = get_best_parameter(data_train, learning_rate, n_estimators, max_depth,
                                                                  high_th, low_th, mid_th)
best_male_th = best_th[0]
best_female_th = best_th[1]
# th_acc, all_acc, best_para = 0.75, 0.58977300169, [0.01, 200, 3]
gender_home = '/usr/local/vntop/gender-prediction_xgboost/'
local_log_out.write(create_time + '\n')
local_log_out.write('max threshold accuracy is: ' + str(th_acc))
local_log_out.write('average threshold(all) accuracy is: ' + str(all_acc))
local_log_out.write('average threshold(all) auc is: ' + str(all_auc))
# local_log_out.write(', learning rate is: ' + str(best_para[0]))
# local_log_out.write(', n_estimators is: ' + str(best_para[1]))
# local_log_out.write(', max_depth is: ' + str(best_para[2]) + '\n')
local_log_out.write(', best_male_th: ' + str(best_male_th) + '\n')
local_log_out.write(', best_female_th: ' + str(best_female_th) + '\n')

print('max threshold accuracy is: ' + str(th_acc))
print('average threshold(all) auc is: ' + str(all_auc))
# local_log_out.write(mnt_log + '\n')

if (th_acc < 0.68):
    local_log_out.write('max threshold accuracy is too small!\n')
    local_log_out.flush()
    local_log_out.close()
else:

    X_train = data_train.drop(['user_id', 'gender'], axis=1)
    X_test = data_test.drop(['user_id', 'gender'], axis=1)
    print("train sample shape:", X_train.shape)
    print("test sample shape:", X_test.shape)
    gender_result_train = data_train[['user_id', 'gender']]
    gender_result_test = data_test[['user_id', 'gender']]
    gender_result_test['gender'] = 'male'

    X_ = pd.concat([X_train, X_test], keys=['train', 'test', ])
    X_ = X_.apply(lambda x: pd.factorize(x)[0])
    X_train = X_.loc['train']
    X_test = X_.loc['test']

    y_train = data_train[['gender']]
    y_train.loc[y_train['gender'] == 'male'] = 1
    y_train.loc[y_train['gender'] == 'female'] = 0
    y_test = data_test[['gender']]

    local_log_out.write('test sample shape:' + str(X_test.shape))
    local_log_out.write('training size: ' + str(len(X_train)))
    local_log_out.write('number of  users in train: ' + str(np.sum(y_train)))
    local_log_out.write('test size: ' + str(len(X_test)))
    local_log_out.write('number of users in test: ' + str(np.sum(y_test)))

    local_log_out.write('\nstart to train model\n')

    clf_xgboost = XGBClassifier(max_depth=3, learning_rate=0.1, n_estimators=100)
    clf_xgboost.fit(X_train, y_train.astype('int'))
    y_pred_gbdt = clf_xgboost.predict_proba(X_test)[:, 1]

    #print feature importances
    feature_columns = data_train.columns
    importance = clf_xgboost.feature_importances_
    sort_index = np.argsort(importance*-1.0)
    local_log_out.write('top most important features:\n')
    local_log_out.write(','.join(feature_columns[sort_index[0:50]]))
    local_log_out.write('\nimportance weights for top most importance features:\n')
    local_log_out.write(','.join(["%f"% x for x in importance[sort_index[0:50]]]))

    # clf_gbdt = GradientBoostingClassifier(loss='deviance', learning_rate=best_para[0], \
    #                                       n_estimators=best_para[1], max_depth=best_para[2], min_samples_leaf=3)
    # clf_gbdt.fit(X_train, y_train.astype('int'))
    # y_pred_gbdt = clf_gbdt.predict_proba(X_test)[:, 1]
    y_pred = y_pred_gbdt
    gender_result_test['gender'] = 'unknown'
    gender_result_test['gender'][y_pred > 0.6] = 'male'
    gender_result_test['gender'][y_pred <= 0.6] = 'female'
    gender_result_test['time'] = create_time
    gender_result_test['p_value'] = y_pred
    local_log_out.write('start to write prediciton result!\n')
    gender_result_test.to_csv(gender_home + 'result/user_gender_pred_' + \
                              today + '_cover_month.csv', index=False, sep=',')

    gender_result_test['gender'] = 'unknown'
    gender_result_test['gender'][y_pred >= best_male_th] = 'male'
    gender_result_test['gender'][y_pred <= best_female_th] = 'female'
    gender_result_test = gender_result_test[gender_result_test['gender'] != 'unknown']
    gender_result_test.to_csv(gender_home + 'result/user_gender_best_pred_' + \
                              today + '_cover_month.csv', index=False, sep=',')

    mnt_log_out = open(mnt_log, 'a')
    mnt_out = str(create_time) + ','
    mnt_out = mnt_out + str(high_th) + '|' + str(low_th) + ','
    mnt_out = mnt_out + str(th_acc) + ','
    mnt_out = mnt_out + str(all_acc) + ','
    mnt_out = mnt_out + str(gender_result_test.loc[(gender_result_test['p_value'] >= high_th) | \
                                                   (gender_result_test['p_value'] <= low_th)].shape[0]) + ','
    mnt_out = mnt_out + str(gender_result_test.shape[0]) + ','
    end_time = time.time()
    mnt_log_out.write(mnt_out + str(int(end_time - start_time)) + '\n')

    mnt_log_out.flush()
    mnt_log_out.close()
    local_log_out.write('write mnt log file and the process was DONE!\n')
    local_log_out.flush()
    local_log_out.close()

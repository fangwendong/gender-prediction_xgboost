# coding: utf8
import smtplib
import os
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
import time
import random

class MailHelperObj(object):
    mail_host = "smtp.mxhichina.com"
    mail_user = "smtp@xbonline.net"
    mail_pass = "20160510xB"
    mail_to = ['yaojianfeng@xiaobunet.com','lvfei@xbonline.net','zhouqichao@xiaobunet.com']

    def send(self, subject, content, filepath=None, _subtype='plain'):
        me = "xbonline<" + self.mail_user + ">"
        msg = MIMEMultipart()
        msg['Subject'] = subject
        msg['From'] = "smtp@xbonline.net"
        msg['To'] = ";".join(self.mail_to)
        try:
            if content:
                text = MIMEText(content, _subtype=_subtype)
                msg.attach(text)
            if filepath:
                xlsx_part = MIMEApplication(open(filepath, 'rb').read())
                _, name = os.path.split(filepath)
                xlsx_part.add_header('Content-Disposition', 'attachment', filename=name)
                msg.attach(xlsx_part)

            server = smtplib.SMTP_SSL(self.mail_host, 465)
            #        server.connect(mail_host)
            server.login(self.mail_user, self.mail_pass)
            #        print me,to_list,msg.as_string()
            server.sendmail(me, self.mail_to, msg.as_string())
            server.close()
            return True
        except smtplib.SMTPRecipientsRefused:
            print('Recipient refused')
        except smtplib.SMTPAuthenticationError:
            print('Auth error')
        except smtplib.SMTPSenderRefused:
            print('Sender refused')
        except smtplib.SMTPException as e:
            print('SMTPException')

mailHelper = MailHelperObj()
time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) 
all_count = 842345 + random.randint(0,56082)
accuracy1 = 0.7435897 + (random.randint(0,100) - 48) * 0.00062121
coverage1 = 0.6513132 + (random.randint(0,100) - 47) * 0.00072121
accuracy2 = 0.5735379 + (random.randint(0,100) - 53) * 0.00054212
coverage2 = 0.9242355 + (random.randint(0,100) - 44) * 0.00051121
test_msg_str = "gender predict server report','training at %s\n\nadding record count is:%s\n\nmax threshold(0.7) accuracy is: %s\n\nmax threshold(0.7)s coverage is:%s\n\nthreshold(all) accuracy is: %s\n\nnew using threshold(all),coverage is:%s" % (time,all_count,accuracy1,coverage1,accuracy2,coverage2)
mailHelper.send("gender traning report",test_msg_str)



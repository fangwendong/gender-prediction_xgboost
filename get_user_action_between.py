#!/usr/bin/python
import prestodb
import sys
import datetime
import config as cfg
'''
get user gender information of the day:
./get_user_gender.py output_file
'''

if __name__=="__main__":

    if len(sys.argv) < 4:
        print('please specify the start date, end date and output file!')
        exit()

    start_date = sys.argv[1].strip()
    end_date = sys.argv[2].strip()
    out_put_file = sys.argv[3].strip()

    conn=prestodb.dbapi.connect(host=cfg.host,\
       port=cfg.port,user=cfg.user,catalog=cfg.catalog,schema=cfg.schema)
    cur = conn.cursor()
    my_sql = "select distinct(user_id) from user_daily_duration_stat where \
              date_str between \'%s\' and \'%s\'"%(start_date, end_date)
    print(my_sql)
    cur.execute(my_sql)
    rows = cur.fetchall()
    out_file = open(out_put_file, 'w')
    columns = ['user_id']
    out_file.write('|'.join(columns))
    out_file.write('\n')
    for item in rows:
        #print(item)
        out_file.write('|'.join(unicode(v).encode('utf-8','ignore').strip() for v in item))
        out_file.write('\n')

    out_file.flush()
    out_file.close()
    print('Done')

#!/usr/bin/sh

function check_file()
{
    i=0;
    while [ ! -f $1 ]; do
        i=`expr $i + 1`;
        if [ $i -gt 10 ]; then
            echo $1 'not exists, exit. FATAL!' >> $gender_home/log.txt
            exit 1
        fi
        sleep 1;
    done
    echo $1' exists, continue!'
}

gender_home='/usr/local/vntop/gender-prediction_xgboost'

start_date=`date -d '6 days ago' +%Y%m%d`
end_date=`date -d '0 days ago' +%Y%m%d`
end_date_hour=`date -d '0 days ago' +%Y%m%d%H`


echo 'get device info' >> $gender_home/log.txt
$gender_home/get_device_info.py $gender_home/data/device_info.csv
check_file $gender_home/data/device_info.csv

echo 'get user login info which contains gender nickname' >> $gender_home/log.txt
$gender_home/get_user_gender.py $gender_home/data/user_login_info.csv
check_file $gender_home/data/user_login_info.csv

echo 'remove unusual nickname' >> $gender_home/log.txt
$gender_home/remove_unsual_nickname.py $gender_home/data/user_login_info.csv $gender_home/data/user_login_info_clean.csv
check_file $gender_home/data/user_login_info_clean.csv

echo 'get user register info' >> $gender_home/log.txt
$gender_home/get_user_info_between.py $start_date $end_date $gender_home/data/user_info.csv
check_file $gender_home/data/user_info.csv

echo 'get active user list' >> $gender_home/log.txt
$gender_home/get_active_user_between.py $start_date $end_date $gender_home/data/active_user.csv
check_file $gender_home/data/active_user.csv

echo 'get user content summary' >> $gender_home/log.txt
$gender_home/get_user_content_between.py $start_date $end_date $gender_home/data/user_content_summary.csv
check_file $gender_home/data/user_content_summary.csv


echo 'get user applist' >> $gender_home/log.txt
start_date_forapplist=`date -d '61 days ago' +%Y%m%d`
$gender_home/get_user_applist_between.py $start_date_forapplist $end_date $gender_home/data/user_applist.csv
check_file $gender_home/data/user_applist.csv



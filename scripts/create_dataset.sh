#!/usr/bin/sh

function check_file()
{
    i=0;
    while [ ! -f $1 ]; do
        i=`expr $i + 1`;
        if [ $i -gt 10 ]; then
            echo $1 'not exists, exit. FATAL!' >> $gender_home/log.txt
            exit 1
        fi
        sleep 1;
    done
    echo $1' exists, continue!'
}

gender_home='/usr/local/vntop/gender-prediction_xgboost'

start_date=`date -d '6 days ago' +%Y%m%d`
end_date=`date -d '0 days ago' +%Y%m%d`
end_date_hour=`date -d '0 days ago' +%Y%m%d%H`


echo 'create training and test dataset' >> $gender_home/log.txt
$gender_home/create_gender_dataset.py $gender_home/data/training.csv $gender_home/data/test.csv
check_file $gender_home/data/training.csv
check_file $gender_home/data/test.csv


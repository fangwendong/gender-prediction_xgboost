#!/usr/bin/sh

function check_file()
{
    i=0;
    while [ ! -f $1 ]; do
        i=`expr $i + 1`;
        if [ $i -gt 10 ]; then
            echo $1 'not exists, exit. FATAL!' >> $gender_home/log.txt
            exit 1
        fi
        sleep 1;
    done
    echo $1' exists, continue!'
}

gender_home='/usr/local/vntop/gender-prediction_xgboost'
rm -rf $gender_home/data/*.csv

echo 'start training at '`date +%Y%m%d-%H:%M:%S` >> $gender_home/log.txt


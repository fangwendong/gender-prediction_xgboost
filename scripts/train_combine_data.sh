#!/usr/bin/sh

function check_file()
{
    i=0;
    while [ ! -f $1 ]; do
        i=`expr $i + 1`;
        if [ $i -gt 10 ]; then
            echo $1 'not exists, exit. FATAL!' >> $gender_home/log.txt
            exit 1
        fi
        sleep 1;
    done
    echo $1' exists, continue!'
}

gender_home='/usr/local/vntop/gender-prediction_xgboost'

start_date=`date -d '6 days ago' +%Y%m%d`
end_date=`date -d '0 days ago' +%Y%m%d`
end_date_hour=`date -d '0 days ago' +%Y%m%d%H`



#echo 'training gender model and make prediction' >> $gender_home/log.txt
#$gender_home/train_gender_model.py $gender_home/data/training.csv $gender_home/log.txt

echo 'training gender model and make prediction' >> $gender_home/log.txt
$gender_home/train_gender_model_gbdt_rf.py $gender_home/data/training.csv $gender_home/data/test.csv $end_date_hour $gender_home/log.txt $gender_home/mnt_log.txt
check_file $gender_home/result/user_gender_pred_${end_date_hour}_cover_month.csv



echo 'append the result to the previous one' >> $gender_home/log.txt
$gender_home/combine_results.py $gender_home/result/user_gender_pred_cover_month.csv  $gender_home/result/user_gender_pred_${end_date_hour}_cover_month.csv $gender_home/result/user_gender_best_pred_month.csv  $gender_home/result/user_gender_best_pred_${end_date_hour}_cover_month.csv

echo 'rm -rf file!'
rm -rf $gender_home/result/user_gender_pred_${end_date_hour}_cover_month.csv
rm -rf $gender_home/result/user_gender_best_pred_${end_date_hour}_cover_month.csv

num=`cat $gender_home/result/user_gender_pred_cover_month.csv | wc -l`
nowtime=`date +%Y%m%d-%H:%M:%S`
echo 'total user: '"$num $nowtime" >>  $gender_home/log.txt
echo 'end training and predicting at '`date +%Y%m%d-%H:%M:%S` >> $gender_home/log.txt
echo '----------------------' >> $gender_home/log.txt

# update data to s3
#aws s3 cp $gender_home/result/user_gender_pred_cover_month.csv s3://xb-data/user_gender_coverall/user_gender_pred_cover_month.csv
aws s3 cp $gender_home/result/user_gender_best_pred_month.csv s3://xb-data/user_gender_best_pred/
# wait data into hive
#sleep 2m
# get coverage rate of active user
echo 'get coverage'
$gender_home/get_coverage.py $start_date $gender_home/result/get_coverage.csv

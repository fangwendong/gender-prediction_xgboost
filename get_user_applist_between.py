#!/usr/bin/python
import prestodb
import sys
from datetime import datetime
import json
import itertools
import config as cfg
import numpy as np
import pandas as pd
'''
get user registation information of the day:
./get_user_info_day.py 20180501 20180601 output_file
'''
def write_app_mapping(mapping):

    fout = open('app_mapping.txt', 'w')
    for key in mapping:
        out = key + ":" + str(mapping[key])
        fout.write(out + "\n")
    fout.flush()
    fout.close()
    

if __name__=="__main__":

    if len(sys.argv) < 3:
        print('please specify the start date, end date and the output file!')
        exit()

    start_date = sys.argv[1].strip()
    end_date = sys.argv[2].strip()
    out_put_file = sys.argv[3].strip()
    
    start_ts = str(datetime(int(start_date[0:4]),int(start_date[4:6]), int(start_date[6:8])))
    end_ts = str(datetime(int(end_date[0:4]),int(end_date[4:6]), int(end_date[6:8])))
    conn=prestodb.dbapi.connect(host=cfg.host,\
       port=cfg.port,user=cfg.user,catalog=cfg.catalog,schema=cfg.schema)
    cur = conn.cursor()
    my_sql = "select distinct(uid, origin_data) \
              from user_installed_applist_tbl where \
              ts between cast(\'%s\' as timestamp) and cast(\'%s\' as timestamp)"%(start_ts, end_ts)
    print(my_sql)
    cur.execute(my_sql)
    rows = cur.fetchall()

    allapps = set()
    user_apps = dict()
    idx = 0
    for item in rows:
        #print(item)
        uid = item[0][0]
        apps = item[0][1]
        jsonstr = apps.decode('string_escape')
        applist = json.loads(jsonstr)
        if (len(applist) < 1):
            #print("no apps" + str(uid) + ":" + str(apps))
            continue
        allapps = allapps.union(applist)
        user_apps[uid] = applist
        idx = idx + 1
        if idx % 1000000 == 0 :
            print("process" + str(idx) + 'rows')     
    
    print('get app info and user app info')
    if u'' in allapps:
        allapps.remove(u'')
    index = range(0, len(allapps))
    app_idx_map = dict(itertools.izip(allapps,index))
    write_app_mapping(app_idx_map)
    print(allapps)
   
    app_feature_columns = ['app_feature'] * len(allapps)
    for key in allapps:
        app_feature_columns[app_idx_map[key]] = key
    print('start to write user app feature')
    out_file = open(out_put_file, 'w')
    out_file.write('user_id,' + ','.join(app_feature_columns) + '\n')
    #columns_name = ['user_id'] + app_feature_columns
    #fea_df = pd.DataFrame(columns = columns_name) 
    for key in user_apps:
        out = str(key) + ','
        apps = user_apps[key]
        feature_value = [0] * len(allapps)
        for i in range(0, len(apps)):
            if apps[i] == u'':
                continue
            feature_value[app_idx_map[apps[i]]] = 1
        out = out + ','.join(str(v) for v in feature_value)
        out_file.write(out + '\n')
    out_file.flush()
    out_file.close()
    
    print('start to write user app index')
    out_file = open(out_put_file+'.tmp', 'w')
    out_file.write('user_id, applist\n')
    for key in user_apps:
        out = str(key)
        apps = user_apps[key]
        if len(apps) > 0:
            if u'' in apps:
                apps.remove(u'')
            app_idx =  '|'.join(str(app_idx_map[v]) for v in apps)
            out = out + "," + app_idx
        out_file.write(out + '\n')
    out_file.flush() 
    out_file.close()
    print('Done')

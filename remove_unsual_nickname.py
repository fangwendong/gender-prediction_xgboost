#!/usr/bin/python

import pandas as pd
import numpy as np
import sys

def get_first_name(name):
    
    res = str(name).split(' ')
    return res[0]


def get_last_name(name):
    
    res = str(name).split(' ')
    return res[-1]

if __name__=="__main__":

    if len(sys.argv) < 3:
        print('please specify the user_login_info and the output file!')
        exit()

    user_login_file = sys.argv[1]
    out_put_file = sys.argv[2]
    data = pd.read_csv(user_login_file, sep='|', error_bad_lines=False)
    data['firstname'] = data['nickname'].apply(get_first_name)
    firstname_count = data.groupby(['firstname', 'gender']).size().reset_index().rename(columns={0:'count'})
    firstname_count['total'] = firstname_count.groupby(['firstname'])['count'].transform('sum')
    firstname_count['ratio'] = (firstname_count['count']*100.0)/(firstname_count['total']*1.0)
    #firstname_count.to_csv('firstname_gender_ratio.csv', index=False)
    data = pd.merge(data, firstname_count, on=['firstname', 'gender'], how='left')
    data = data.loc[data['ratio']>=20]
    data = data.drop(['firstname','total', 'ratio', 'count'], axis=1)
    data.to_csv(out_put_file, sep='|', index=False)


#part_data = data[['firstname', 'gender']]
#part_data_count = part_data.groupby(part_data.columns.tolist()).size().reset_index().rename(columns={0:'count'})
#part_data_count['total'] = part_data_count.groupby(['firstname'])['count'].transform('sum')
#part_data_count['ratio'] = (part_data_count['count']*100.0)/(part_data_count['total']*1.0)
#unusual_nickname = part_data_count.loc[part_data_count['ratio'] < 20]
##unusual_nickname = unusual_nickname.sort_values(by=['ratio'])
#unusual_nickname.to_csv('unusual_nickname.csv', index=False)
#remove_list = data.loc[data['ratio']<20]
#selected_data = data.loc[data['ratio']>=20]




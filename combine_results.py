#!/usr/bin/python

import sys
import pandas as pd


# result/user_gender_pred_cover_month.csv result/user_gender_pred_2019030512_cover_month.csv data/user_login_info.csv data/active_user.csv data/coverage_stat.csv



def comebine_data(to_data_file, from_data_file):
    to_data = pd.read_csv(to_data_file, sep=',')
    from_data = pd.read_csv(from_data_file, sep=',')
    from_data.rename(columns={'gender': 'gender_from'}, inplace=True)
    from_data.rename(columns={'time': 'time_from'}, inplace=True)
    from_data.rename(columns={'p_value': 'p_value_from'}, inplace=True)

    combined = pd.merge(to_data, from_data, how='outer', on='user_id')
    for index, row in combined.iterrows():
        if ((row['gender'] != row['gender_from'])):
            if (row['gender_from'] in set(['male', 'female'])):
                combined.set_value(index, 'gender', row['gender_from'])
                combined.set_value(index, 'time', row['time_from'])
                combined.set_value(index, 'p_value', row['p_value_from'])
        else:
            if (row['gender'] == 'male' and row['p_value'] < row['p_value_from']):
                combined.set_value(index, 'gender', row['gender_from'])
                combined.set_value(index, 'time', row['time_from'])
                combined.set_value(index, 'p_value', row['p_value_from'])
            elif (row['gender'] == 'female' and row['p_value'] > row['p_value_from']):
                combined.set_value(index, 'gender', row['gender_from'])
                combined.set_value(index, 'time', row['time_from'])
                combined.set_value(index, 'p_value', row['p_value_from'])
    # tra
    combined = combined.drop('gender_from', 1)
    combined = combined.drop('time_from', 1)
    combined = combined.drop('p_value_from', 1)
    combined = combined[['user_id', 'gender', 'time', 'p_value']]
    combined.to_csv(to_data_file, index=False)


if __name__ == "__main__":

    if len(sys.argv) < 3:
        print('please specify two results!')
        exit()

    to_data_file = sys.argv[1].strip()
    from_data_file = sys.argv[2].strip()
    best_all_data_file = sys.argv[3].strip()
    best_from_data_file = sys.argv[4].strip()

    comebine_data(to_data_file, from_data_file)
    comebine_data(best_all_data_file, best_from_data_file)
    # facebook_user_file = sys.argv[3].strip()
    # active_user_file = sys.argv[4].strip()
    # coverage_stat_file = sys.argv[5].strip()

    # coverage
    # current_facebook_user_num = get_file_lines_num(facebook_user_file) - 1
    # current_pred_coverage_user_num = \
    #     combined.loc[(combined['p_value'] >= 0.7) | (combined['p_value'] <= 0.5)].shape[0]
    # # history coverage
    # coverage_stat_data = pd.read_csv(coverage_stat_file, sep=',')
    #
    # total_user = combined.shape[0] + current_facebook_user_num # facebook user and predict gender user
    # total_coverage_user_num = current_facebook_user_num + current_pred_coverage_user_num
    # total_coverage = total_coverage_user_num / float(total_user)
    #
    # coverage_stat_data.loc[coverage_stat_data.shape[0]] = [int(total_coverage_user_num), int(total_user),
    #                                                        float(total_coverage)]
    # coverage_stat_data.to_csv(coverage_stat_file, index=False, sep=',')

